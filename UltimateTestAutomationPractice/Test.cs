using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;

namespace UltimateTestAutomationPractice
{
    public class Test
    {
        public IWebDriver driver;

        [Test]
        public void Practice()
        {
            Assert.IsTrue(true);
        }

        [Test]
        public void SampleTest()
        {
            driver = new ChromeDriver("C:/Users/esteen/testAutomation/webDriver");
            driver.Manage().Window.Maximize();
            driver.Quit();
        }

        [Test]
        public void SampleTest2()
        {
            driver = new ChromeDriver("C:/Users/esteen/testAutomation/webDriver");
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://ultimateqa.com/automation/");
            driver.Quit();   
        }
    }
}